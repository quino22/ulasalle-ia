import numpy as np
import matplotlib.pyplot as plt
import datetime



class RegresionLineal:
    err = 0
    __alpha = 0.05
    pointX = None
    pointX = None
    probabilidad = None

    def __init__(self, X, Y):
        self.pointX = X
        self.pointY = Y
        self.theta_0 = 0
        self.theta_1 = 0
        self.h = np.zeros(len(X))

    def execute(self, umbral):
        self.theta_0 = 0 # np.random.random()
        self.theta_1 = 0 # np.random.random()
        self.funcion_recta(self.pointX)
        self.probabilidad_func()
        self.encontrar_error()
        cont = 0
        while self.err > umbral:
            # self.draw(cont)
            self.funcion_recta(self.pointX)
            #self.probabilidad_func()
            self.modifitar_theta()
            self.encontrar_error()
            self.probabilidad_func()
            cont += 1
            #if cont ==  10:
            #    return


    def funcion_recta(self, pointx):
        self.h = [self.theta_0 * 1 + self.theta_1 * e for e in pointx]
        #print("RECTA: ", self.h)


    def probabilidad_func(self):
        self.probabilidad = [ 1 / (1 + np.exp ( -e ) ) for e in self.h ]
        #self.probabilidad = np.asarray(self.probabilidad)
        print(self.probabilidad)


    def predecir(self,value):
        return 1 / (1 + np.exp(value))


    def encontrar_error(self):
       

        self.err = -1 * (np.sum([ (e[0] * np.log(e[1])) + ((1 - e[0]) * np.log(1 - e[1])) for e in zip(self.pointY,self.probabilidad)])) / len(self.pointY)
        print("------>", self.err)


    def modifitar_theta(self):

        # self.theta_0 = self.theta_0 - self.__alpha * (np.sum((h - pointy) * 1)) / (len(h))
        # self.theta_1 = self.theta_1 - self.__alpha * (np.sum((h - pointy) * pointx)) / (len(h))

        self.theta_0 = self.theta_0 - self.__alpha * (np.sum((self.probabilidad - self.pointY) * 1))
        self.theta_1 = self.theta_1 - self.__alpha * (np.sum((self.probabilidad - self.pointY) * self.pointX))

        #print("T0: " , self.theta_0)
        #print("T1: " , self.theta_1)


    def draw(self, cont):
        fig = plt.figure()
        plt.plot(self.pointX, self.pointY, 'ro')
        plt.plot(self.h)
        plt.axis([0, 1000, 0, 1000])
        plt.show()
        fig.savefig('imagen_' + str(cont) + '.png')  # Use fig. here


if __name__ == '__main__':

    x = np.array([
        13,
        69,
        72,
        11,
        47,
        51,
        91,
        65,
        42,
        40,
        94,
        99,
        53,
        92,
        61,
        28,
        78,
        61,
        86,
        97,
        57,
        81,
        100,
        77,
        63,
        32,
        13,
        68,
        57,
        63,
        18,
        76,
        43,
        89,
        16,
        14,
        27,
        62,
        20,
        87,
        8,
        27,
        92,
        4,
        97,
        4,
        18,
        40,
        14,
        80,
        49,
        7,
        97,
        95,
        63,
        16,
        77,
        78,
        68,
        48,
        39,
        63,
        12
    ])

    y = np.array([
        0,
        1,
        1,
        0,
        0,
        1,
        1,
        1,
        0,
        0,
        1,
        1,
        1,
        1,
        1,
        0,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        1,
        0,
        0,
        1,
        1,
        1,
        0,
        1,
        0,
        1,
        0,
        0,
        0,
        1,
        0,
        1,
        0,
        0,
        1,
        0,
        1,
        0,
        0,
        0,
        0,
        1,
        0,
        0,
        1,
        1,
        1,
        0,
        1,
        1,
        1,
        0,
        0,
        1,
        0
    ])
    print(str(datetime.datetime.now().time()))

    lr = RegresionLineal(x, y)
    lr.execute(0.3)
    print(lr.predecir(80))
    print(str(datetime.datetime.now().time()))
    print("ERROR => ", lr.err)








