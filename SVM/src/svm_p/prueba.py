print(__doc__)

# Code source: Gaël Varoquaux
# License: BSD 3 clause

import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm
import pandas as pd

data = pd.read_csv('/home/joaquin/Documentos/Ulasalle/IA/SVM/src/haar_c/entrenamiento.txt', sep=",", header=None)
arr = np.asarray(data)
print("DATOS DE ENTRENAMIENTO")
print(data)
X = arr[: ,:8]
Y = arr[: , -1]

test = pd.read_csv('/home/joaquin/Documentos/Ulasalle/IA/SVM/src/haar_c/test.txt', sep=",", header=None)
arr_test = np.asarray(test)

X_test = arr_test[: ,:8]
print("DATOS DE PRUEBA")
print(test)
clf = svm.SVC(kernel='poly', gamma=3)
clf.fit(X, Y)

for x in X_test:
    r =  clf.predict([x]) # 0
    if (r == 1):
        print("gato")
    elif (r == 2 ):
        print("perro")
    else:
        print("pato")

