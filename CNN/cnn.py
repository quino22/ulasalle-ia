from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
from keras import backend as K
from keras.preprocessing import image
import numpy as np

# dimensions of our images.
img_width, img_height = 64,64

#train_data_dir = '/home/joaquin/Escritorio/data/train'
#test_data_dir = '/home/joaquin/Escritorio/data/test'

train_data_dir = '/home/joaquin/Descargas/data/train'
test_data_dir = '/home/joaquin/Descargas/data/test'

train_data = 152
test_data = 23
epochs = 3
batch_size = 16

# if K.image_data_format() == 'channels_first':
#     input_shape = (3, img_width, img_height)
# else:

input_shape = (img_width, img_height, 3)


model = Sequential()

model.add(Conv2D(32, (3, 3), input_shape=input_shape , activation="relu"))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(32, (3, 3), activation="relu"))
model.add(MaxPooling2D(pool_size=(2, 2)))


model.add(Flatten())
model.add(Dense(activation="relu", units=128))
#model.add(Dropout(0.5))
model.add(Dense(activation="sigmoid", units=1))

model.compile(loss='binary_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

# this is the augmentation configuration we will use for training
train_datagen = ImageDataGenerator(
    rescale=1. / 255,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip = True)
                                                                                                        
# this is the augmentation configuration we will use for testing:
# only rescaling
test_datagen = ImageDataGenerator(rescale=1. / 255)

train_generator = train_datagen.flow_from_directory(
    train_data_dir,
    target_size=(img_width, img_height),
    batch_size=batch_size,
    class_mode='binary',
)


#print( img_width, img_height)   

validation_generator = test_datagen.flow_from_directory(
    test_data_dir,
    target_size=(img_width, img_height),
    batch_size=batch_size,
    class_mode='binary'
)

model.fit_generator(
    train_generator,
    steps_per_epoch=(4000/32),#train_data // batch_size,
    epochs=epochs,
    validation_data=validation_generator,
    validation_steps=test_data // batch_size)



path = '/home/joaquin/Descargas/data/original-images/bunny/enhanced-buzz-517-1296592131-31.jpg'
test_image = image.load_img(path, target_size=(img_width, img_height))
test_image = image.img_to_array(test_image)
test_image = np.expand_dims(test_image, axis = 0)
result = model.predict(test_image)
print(result)


path = '/home/joaquin/Descargas/data/original-images/puppy/Cute-puppy-photos-82.jpg'
test_image = image.load_img(path, target_size=(img_width, img_height))
test_image = image.img_to_array(test_image)
test_image = np.expand_dims(test_image, axis = 0)
result = model.predict(test_image)
print(result)


path = '/home/joaquin/Descargas/data/original-images/puppy/Cute-puppy-photos-662.jpg'
test_image = image.load_img(path, target_size=(img_width, img_height))
test_image = image.img_to_array(test_image)
test_image = np.expand_dims(test_image, axis = 0)
result = model.predict(test_image)
print(result)
