# -*- coding: utf-8 -*-
# Form implementation generated from reading ui file 'untitled.ui'
#
# Created by: PyQt5 UI code generator 5.8.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        ###########################
        #MAIN WINDOW
        ###########################
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(911, 567)
        ###########################
        #FONTS
        ###########################
        fontW = QtGui.QFont()
        fontW.setPointSize(12)
        fontW.setBold(True)
        fontW.setWeight(75)

        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)


        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(20, 10, 301, 31))
        
        self.label.setFont(fontW)
        self.label.setObjectName("label")

        ###########################        
        #caja de Opciones
        #Numero de columnas y filas 
        ###########################

        self.optionBox = QtWidgets.QGroupBox(self.centralwidget)
        self.optionBox.setGeometry(QtCore.QRect(620, 40, 271, 91))
        
        self.optionBox.setFont(font)
        self.optionBox.setObjectName("optionBox")

        self.lbl_row= QtWidgets.QLabel(self.optionBox)
        self.lbl_row.setGeometry(QtCore.QRect(10, 30, 50, 20))
        self.lbl_row.setFont(font)
        self.lbl_row.setObjectName("lbl_row")

        self.lbl_col= QtWidgets.QLabel(self.optionBox)
        self.lbl_col.setGeometry(QtCore.QRect(10, 60, 80, 22))
        self.lbl_col.setFont(font)
        self.lbl_col.setObjectName("lbl_col")

        self.in_row = QtWidgets.QLineEdit(self.optionBox)
        self.in_row.setGeometry(QtCore.QRect(100, 30, 101, 29))
        self.in_row.setObjectName("in_row")

        self.in_col= QtWidgets.QLineEdit(self.optionBox)
        self.in_col.setGeometry(QtCore.QRect(100, 60, 101, 29))
        self.in_col.setObjectName("in_col")


        ###########################
        #Gafico
        ###########################
        self.graphicsView = QtWidgets.QGraphicsView(self.centralwidget)
        self.graphicsView.setGeometry(QtCore.QRect(10, 40, 601, 411))
        self.graphicsView.setObjectName("graphicsView")

        ###########################
        #boton Limpiar
        ###########################

        self.btn_clear = QtWidgets.QPushButton(self.centralwidget)
        self.btn_clear.setGeometry(QtCore.QRect(10, 470, 88, 29))
        self.btn_clear.setObjectName("btn_clear")


        ###########################
        #caja de entrenamiento 
        #
        ###########################

        self.trainbox = QtWidgets.QGroupBox(self.centralwidget)
        self.trainbox.setGeometry(QtCore.QRect(620, 150, 271, 150))

        self.trainbox.setFont(font)
        self.trainbox.setObjectName("trainbox")

        self.btn_path = QtWidgets.QPushButton(self.trainbox)
        self.btn_path.setGeometry(QtCore.QRect(80, 80, 150, 29))
        self.btn_path.setObjectName("btn_path")

        self.lbl_file = QtWidgets.QLabel(self.trainbox)
        self.lbl_file.setGeometry(QtCore.QRect(10, 80, 56, 31))
        
        self.lbl_file.setFont(font)
        self.lbl_file.setObjectName("lbl_file")

        self.btn_train = QtWidgets.QPushButton(self.trainbox)
        self.btn_train.setGeometry(QtCore.QRect(40, 40, 161, 29))
        self.btn_train.setObjectName("btn_train")


        self.output = QtWidgets.QLabel(self.trainbox)
        self.output.setGeometry(QtCore.QRect(10, 130, 271, 17))
        self.output.setText("dsd")
        self.output.setObjectName("output")
        self.output_2 = QtWidgets.QLabel(self.trainbox)
        self.output_2.setGeometry(QtCore.QRect(10, 150, 271, 17))
        self.output_2.setText("")
        self.output_2.setObjectName("output_2")
        self.testbox = QtWidgets.QGroupBox(self.centralwidget)
        self.testbox.setGeometry(QtCore.QRect(620, 330, 271, 171))

        self.testbox.setFont(font)
        self.testbox.setObjectName("testbox")
        self.pushButton = QtWidgets.QPushButton(self.testbox)
        self.pushButton.setGeometry(QtCore.QRect(50, 40, 161, 29))
        self.pushButton.setObjectName("pushButton")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 911, 23))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "RNA: Numeros "))
        self.optionBox.setTitle(_translate("MainWindow", "Opciones:"))
        self.lbl_row.setText(_translate("MainWindow", "Filas: "))
        self.lbl_col.setText(_translate("MainWindow", "Columnas: "))

        self.btn_clear.setText(_translate("MainWindow", "Limpiar"))
        self.btn_path.setText(_translate("MainWindow", "Seleccionar"))
        self.trainbox.setTitle(_translate("MainWindow", "Train:"))
        self.lbl_file.setText(_translate("MainWindow", "Archivo: "))


        self.btn_train.setText(_translate("MainWindow", "Entrenar"))
        self.testbox.setTitle(_translate("MainWindow", "Test:"))
        self.pushButton.setText(_translate("MainWindow", "Start Test"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
