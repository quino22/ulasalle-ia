from PyQt5 import QtCore, QtGui, QtWidgets
from win_kohonen import Ui_MainWindow
import numpy as np
import time

from random import randint
from kohonen import Kohonen, Celula


class MyFirstGuiProgram(QtWidgets.QMainWindow, Ui_MainWindow):

    k = None
    def __init__(self, parent=None):
        QtWidgets.QMainWindow.__init__(self, parent=parent)

        self.setupUi(self)
        self.scene = QtWidgets.QGraphicsScene()
        self.graphicsView.setScene(self.scene)
        
        self.btn_matrix_create.clicked.connect(self.create_Matrix)
        self.btn_clear.clicked.connect(self.clear)
        self.btn_train.clicked.connect(self.train)
        self.side = 20
       

    def clear(self):
        for i in self.scene.items():
            self.scene.removeItem(i)
        #self.scene.clear()

    def create_Matrix(self):
        

        row = int(self.in_row.text())
        col = int(self.in_col.text())
        iter = int(self.in_iter.text())
        radio = int(self.in_radio.text())
        
        self.k = Kohonen(iter,row,col,radio)
        self.pintar(row,col)
        

    def train(self):
        print("Inicio")
        a = 0
        c = Celula()
        completed = 0
        self.progressBar.setValue(completed)
        

        while a <= self.k.numIter:
            completed = (a*100)/self.k.numIter
            self.progressBar.setValue(completed)
            self.k.constTime()
            self.k.learnigRate()
            self.k.radius_neighborhood()
            self.k.newWeight(c)
            self.k.tiempo += 1
            a += 1

            self.scene = QtWidgets.QGraphicsScene()
            self.graphicsView.setScene(self.scene)

            self.pintar(self.k.dimX,self.k.dimY)
        
        print("fin")

    
    def pintar(self,row,col):
        matrix = self.k.matriz
        self.scene.update()
        for i in range(row):
            for j in range(col):
                r = matrix[i][j].color[0]
                g = matrix[i][j].color[1]
                b = matrix[i][j].color[2]
                if r < 0: r = 0
                if g < 0: g = 0
                if b < 0: b = 0

                if r > 255: r = 255
                if g > 255: g = 255
                if b > 255: b = 255

                color = QtGui.QColor(r,g,b)
                pen = QtGui.QPen(color)
                brush = QtGui.QBrush(color)
                x = matrix[i][j].poscicion[0]
                y = matrix[i][j].poscicion[1]
                
                r = QtCore.QRectF(QtCore.QPointF(x*self.side, y*self.side), QtCore.QSizeF(self.side, self.side))
                self.scene.addRect(r, pen,brush)


if __name__ == '__main__':
    import sys, random
    app = QtWidgets.QApplication(sys.argv)
    w = MyFirstGuiProgram()
    w.show()
    sys.exit(app.exec_())
