import numpy as np
from random import randint
import matplotlib.pyplot as plt
from math import sqrt, exp, pow, log


class Celula:

    color = None
    poscicion = None

    def __init__(self,pos : list = None):
        self.color = [randint(0,255),randint(0,255),randint(0,255)]
        self.poscicion = pos


    def Distancia(self, celula : 'Celula') -> float:
        d = sqrt(sum([(d[1]-d[0])**2 for d in zip(self.color,celula.color)]))
        return  d , celula


    def Distancia_posicion(self, celula : 'Celula') -> float:
        d = sqrt( pow(self.poscicion[0] - celula.poscicion[0],2) + pow(self.poscicion[1] - celula.poscicion[1],2))
        return  d
    
    

class Kohonen:

    matriz = None
    tiempo = 1 
    dimX = 0
    dimY = 0
    numIter = 0
    radio = 0
    radioNeighborhood = 0
    const_Time = 0
    const_Rate = 0
    rate = 0


    def __init__(self,iter : int,x : int ,y: int, r: int):
        self.matriz = None
        self.numIter = iter
        self.dimX = x
        self.dimY = y
        self.const_Rate = r
        self.radio = max(self.dimX, self.dimY)/2
        self.matriz = np.empty((x,y), dtype=Celula)

        self.CreateMatrix()


    def aprender(self):
        a = 0
        c = Celula()
        while a < self.numIter:
            self.constTime()
            self.learnigRate()
            self.radius_neighborhood()
            self.newWeight(c)
            self.tiempo += 1
            a += 1


    def CreateMatrix(self):

        for x in range(self.dimX):
            for y in range(self.dimY):
                self.matriz[x][y] = Celula([x,y])
        

    def getWinner(self,input: list) -> float:

        winner,cellWinner = input.Distancia(self.matriz[0][0])

        for x in range(self.dimX):
            for y in range(self.dimY):
                if winner > input.Distancia(self.matriz[x][y])[0]:
                    winner,cellWinner = input.Distancia(self.matriz[x][y])

        return cellWinner 


    def radius_neighborhood(self):
        self.radioNeighborhood = self.radio * exp(-self.tiempo/self.const_Time)

    
    def constTime(self):
        self.const_Time = self.numIter/log(self.radio)
        
        
    def newWeight(self,input):
        winner = self.getWinner(input)
        self.radius_neighborhood()
        for x in range(self.dimX):
            for y in range(self.dimY):
                celula_actual = self.matriz[x][y]
                dist = winner.Distancia_posicion(celula_actual)
                if dist <= self.radioNeighborhood:
                    distanciaFromWinner = self.distanciaWinner(dist)
                    ic = np.array(input.color)
                    wc = np.array(winner.color)
                    self.matriz[x][y].color = self.matriz[x][y].color + distanciaFromWinner * self.rate * (ic-wc)
        

    def learnigRate(self):
        self.rate = self.const_Rate * exp(-self.tiempo/self.const_Time)


    def distanciaWinner(self,dist : float) -> float:
        p = pow(dist,2)
        q = 2*(pow(self.radioNeighborhood,2))
        return exp(-p / q)


def main():
    k = Kohonen(1000,200,200,10)
    
    print(k.matriz[50][50].color)
    k.aprender()
    print(k.matriz[50][50].color)

if __name__ == '__main__':
    pass
    #main()

        

