#include <iostream>
#include "CImg.h"
#include <vector>
#include <fstream>
#include <climits>

using namespace cimg_library;
using namespace std;

float get_min(CImg<double> & img, int f, int c, int n){

	float minimo = 10000000;

	for (int i = 0;i<f;i++){
		for (int j = 0;j<c;j++){
			if (minimo > img(i,j,n)){
				minimo = img(i,j,n);
			}
		}
	}

	return minimo;
}

float get_max(CImg<double> & img, int f, int c, int n){

	float maximo = 0;

	for (int i = 0;i<f;i++){
		for (int j = 0;j<c;j++){
			if (maximo < img(i,j,n)){
				maximo = img(i,j,n);
			}
		}
	}

	return maximo;
}

vector<int> read_Data(string path, vector<string> & images, string file){
	ifstream inputFile;
	inputFile.open (path+file);
	string data;
	vector<int> clase;
	while(!inputFile.eof()){
		getline(inputFile,data);
		if (data == "") break;
		images.push_back(path+data);
		/*	
		if (data[0] == 'g') clase.push_back(1);
		else if(data[0] == 'p') clase.push_back(2);
		else clase.push_back(0);
		*/
	}
	return clase;
}


vector<vector<float>> Get_caracteristicas(vector<string> img){
	vector<vector<float>> caractersticas;
	for (int i = 0;i< img.size();i++){
		vector<float> c;
		
		CImg<double> image(img[i].c_str());

		float min,max,vari,sd;

		image.resize(128,128);
		image.haar(false,2);
		image.crop(0,0,63,63);
		min = image.min();
		max = image.max();
		vari = image.variance();
		sd = sqrt(image.variance());

		c.push_back(min);
		c.push_back(max);
		c.push_back(vari);
		c.push_back(sd);
		caractersticas.push_back(c);
	}
	

	return caractersticas;

}

int main(int argc,char const *argv[]){

	/*************
	Para ejecutar el programa hay que pasarle 4 argumentos
	1 carpeta de las imagenes,
	2 el nombre del archivo que tiene el nombre de las imagens
	3 el nombre del archivo de salida para entrenar
	4 la clase de cada imagen
	ejm ./har "s1/" "s1.txt" "s1_entrenamiento.txt" "1"
	***************/

	vector<string> images;
	string path = argv[1];
	string file = argv[2];
	string out = argv[3];
	string clas = argv[4];
	vector<int> clase = read_Data(path, images,file);

	vector<vector<float> > car =  Get_caracteristicas(images);
	
	ofstream myfile(out);
	for(int i = 0 ; i < car.size();i++){
		for (int j = 0 ; j < car[i].size();j++){
  			myfile<<car[i][j]<< " , ";
		}
		myfile<<clas<<endl;
		//myfile<<clase[i]<<endl;
	}
	//myfile<<clas<<endl;
	myfile.close();
	
	return 0;
}

