import numpy as np
import pandas as pd
from random import shuffle
import copy

class BP:

    I = None
    Wh = None
    Wo = None
    Sh = None
    So = None
    Sd = None
    Err = 10000000
    Target = None
    Alpha = None
    delta = None

    def __init__(self,NroI,NroH,NroO,target,alpha):
        self.Wh = np.random.rand(NroI,NroH)
        self.Wo = np.random.rand(NroH,NroO)
        #self.Target = target
        #self.Target = np.random.rand(NroO)
        self.Alpha = alpha


    def train(self,I,v):
        self.I = np.matrix(I)
        self.Target = v
        self.Forward(I)
        self.Error()
        self.Backward()
        #while (self.Err > self.Target):
            #self.Forward(I)
            #self.Error()

            
    def prove(self,I,v):
        self.Target = v
        self.I = np.matrix(I)
        self.Forward(I)
        self.Error()
        #print(self.Err)


    def Forward(self,I):
        self.Sh = 1/(1 + np.exp(-1 * np.matmul(I,self.Wh)))
        self.Sh = copy.deepcopy(np.asarray(self.Sh))


        self.So = 1 / ( 1 + np.exp(-1 * np.matmul(self.Sh,self.Wo)))
        self.So = copy.deepcopy(np.asarray(self.So))
        

    def Error(self):
        self.Err = np.sum((np.power(self.Target - self.So,2))/2)
        print("ERROR ====>  " ,self.Err)

    
    def Backward(self):
        tmpWo = self.BackwardOutputLayer()
        self.BackwardHiddenLayer(tmpWo)


    def BackwardOutputLayer(self):
        self.delta = -(self.Target - self.So)*(1 - self.So)*self.So

        tmpSh = np.matrix(self.Sh)
        tmpdelta = np.matrix(self.delta)
        trans = np.transpose(tmpSh)
        tmpWo = copy.deepcopy(self.Wo)

        tmp = np.matmul(trans,tmpdelta)
        self.Wo = self.Wo - self.Alpha * tmp

        return tmpWo

    
    def BackwardHiddenLayer(self,tmpWo):
        
        trans = np.transpose(tmpWo)
        tmp = np.dot(self.delta, trans)

        tmp2 = (np.sum(tmp)) * (1 - self.Sh)*self.Sh        
        tmp2 = np.matrix(tmp2)
        tmp2 = np.transpose(tmp2)

        tmp2 = np.matmul(tmp2,self.I)
        tmp2 = np.transpose(tmp2)     

        self.Wh = self.Wh - (self.Alpha * tmp2)
        


entrenamiento = np.array([])
test = np.array([])
clase = []

path = '../haar_c/'


for x in range(1,11):
    
    data = pd.read_csv(path+'s'+str(x)+'_entrenamiento.txt', sep="," ,header = None,nrows=11)
    arr = np.asarray(data)
    np.random.shuffle(arr)

    e = arr[:6]
    t = arr[-4:]

    if (entrenamiento.size == 0):
        entrenamiento = e
    else:
        entrenamiento = np.concatenate((entrenamiento,e))

    if (test.size == 0):
        test = t
    else:
        test = np.concatenate((test,t))


np.random.shuffle(entrenamiento)
X = entrenamiento[: ,:4]
Y = entrenamiento[:, -1]
i = 0

obj = BP(4, 8, 10, 0.1, 0.01)

while obj.Err > 0.1:
    a = X[i]
    b = np.zeros(10)
    b[int(Y[i]) -1] = 1
    #print(b)
    a = a.astype(float)
    obj.train(a,b)
    i += 1
    if i == 60:
        #print(obj.Err)
        i = 0

print("FIN\n\n")


X = test[:,:4]
Y = test[: , -1]
j = 0
print("TEST")
while True:
    a = X[j]
    b = np.zeros(10)
    b[int(Y[j]) -1] = 1
    #print(b)
    a = a.astype(float)
    obj.prove(a,b)
    # print(b)
    # print(obj.Err)
    # print("-------------------")
    j += 1
    if j == 40:
        break

