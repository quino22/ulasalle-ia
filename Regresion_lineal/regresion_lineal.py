import numpy as np
import matplotlib.pyplot as plt
import datetime
import _tkinter

class RegresionLineal:

    err = 0
    __alpha = 0.0000050
    pointX = None
    pointX = None

    def __init__(self, X, Y):
        self.pointX = X
        self.pointY = Y
        self.theta_0 = 0
        self.theta_1 = 0
        self.h = np.zeros(len(X))


    def execute(self, umbral):
        self.theta_0 = np.random.random()
        self.theta_1 = np.random.random()
        self.funcion_recta(self.pointX)
        self.encontrar_error(self.pointY, self.h)
        cont = 0
        while self.err > umbral:
            self.draw(cont)
            self.funcion_recta(self.pointX)
            self.modifitar_theta(self.pointX, self.pointY, self.h)
            self.encontrar_error(self.pointY, self.h)
            cont += 1


    def funcion_recta(self, pointx):
        self.h = [self.theta_0 * 1  + self.theta_1 * e for e in pointx]


    def encontrar_error(self, pointy, h):
        self.err= np.sum(np.power(pointy - h,2)) / (2*len(pointy))
        print(self.err)

    def modifitar_theta(self, pointx, pointy,h):

        self.theta_0 = self.theta_0 - self.__alpha * (np.sum((h - pointy) * 1)) / (len(h))
        self.theta_1 = self.theta_1 - self.__alpha * (np.sum( (h - pointy) * pointx)) / (len(h))



    def draw(self,cont):
        fig = plt.figure()
        plt.plot(self.pointX, self.pointY, 'ro')
        plt.plot(self.pointX,self.h)
        # plt.axis([0, 1000, 0, 1000])
        #plt.show(block=True)
        fig.savefig('imagen_'+ str(cont)+ '.png')


if __name__ == '__main__':

    
    y = np.array([
    0.1,
    338.8,
    118.1,
    888,
    9.2,
    228.1,
    668.5,
    998.5,
    449.1,
    778.9,
    559.2,
    0.3,
    0.1,
    778.1,
    668.8,
    339.3,
    448.9,
    10.8,
    557.7,
    228.3,
    998,
    888.8,
    119.6,
    0.3,
    0.6,
    557.6,
    339.3,
    888,
    998.5,
    778.9,
    10.2,
    117.6,
    228.9,
    668.4,
    449.2,
    0.2
    ])


    x = np.array([
    0.2,
    337.4,
    118.2,
    884.6,
    10.1,
    226.5,
    666.3,
    996.3,
    448.6,
    777,
    558.2,
    0.4,
    0.6,
    775.5,
    666.9,
    338,
    447.5,
    11.6,
    556,
    228.1,
    995.8,
    887.6,
    120.2,
    0.3,
    0.3,
    556.8,
    339.1,
    887.2,
    999,
    779,
    11.1,
    118.3,
    229.2,
    669.1,
    448.9,
    0.5
    ])

    # x = np.array([2,4,6,8])
    # y = np.array([2,4,6,8])
    print("EMPEZO")
    t  =str(datetime.datetime.now().time())

    lr = RegresionLineal(x, y)
    lr.execute(0.4)
    print("INICIO: ", t )
    print("FIN: ", str(datetime.datetime.now().time()))
    print("ERROR => " , lr.err)








