from random import randint, random
import matplotlib.pyplot as plt


class Cromosoma:

    m_Adn = ""

    def __init__(self,size : int = None):
        if size:
            for i in range(size):
                self.m_Adn+=str(randint(0,1))
        else: 
            self.m_Adn = ""
    

class Individuo:

    m_Cromo = None
    m_Genotipo = None
    m_Fenotipo = None
    m_Probabilidad = None
    m_Probabilidad_Acumulada = None
    size = 0

    def __init__(self,size = None):
        self.m_Cromo = Cromosoma(size)
        if size: 
            self.size = size
            self.m_Genotipo = int(self.m_Cromo.m_Adn, 2) ##x
            self.m_Fenotipo = self.m_Genotipo**2 ###y


    def Calcular_Probalilidad(self,total : int):
        if total == 0:
            total = 0.01
        self.m_Probabilidad = self.m_Fenotipo / total

    def Calcular_Probalilidad_Acumulada(self,probalidad):
        self.m_Probabilidad_Acumulada = probalidad + self.m_Probabilidad
         

    def nuevoIndividuo(self, individuo : str):
        self.m_Cromo.m_Adn = individuo
        self.m_Genotipo = int(self.m_Cromo.m_Adn, 2)
        self.m_Fenotipo = self.m_Genotipo**2


class Poblacion(object):

    #individuos = []
    #m_Fenotipo_Total = 0
    #cant = 0
    #size = 0

    def __init__(self, cant : int, size : int):
        self.cant = cant
        self.size = size
        self.individuos = []
        self.m_Fenotipo_Total = 0 
        
        for i in range(cant):
            i = Individuo(size)
            self.individuos.append(i)
            self.m_Fenotipo_Total += i.m_Fenotipo

    def Calcular_Probalilidades(self):
        pa = 0
        for i in range(len(self.individuos)):
            self.individuos[i].Calcular_Probalilidad(self.m_Fenotipo_Total)
            self.individuos[i].Calcular_Probalilidad_Acumulada(pa)
            pa = self.individuos[i].m_Probabilidad_Acumulada


    def Seleccionar(self) -> object:
        n1 = random()
        n2 = random()

        for i in self.individuos:
            if(i.m_Probabilidad_Acumulada > n1):
                ind = i
                break


        for i in self.individuos:
            if(i.m_Probabilidad_Acumulada > n2):
                ind2 = i
                break

        return ind,ind2


    def CrossOver(self) -> str:
        individuo1, individuo2 = self.Seleccionar()
        tam = len(individuo1.m_Cromo.m_Adn)
        corte = randint(1,tam-1)
        lado = randint(0,1) #0 izq, 1 der
        tmp = list(individuo1.m_Cromo.m_Adn)

        if (lado == 0):
            newInd1 = list(individuo1.m_Cromo.m_Adn)
            newInd2 = list(individuo2.m_Cromo.m_Adn)

            newInd1[:corte] = newInd2[:corte]            
            newInd2[:corte] = tmp[:corte]

        else:
            newInd1 = list(individuo1.m_Cromo.m_Adn)
            newInd2 = list(individuo2.m_Cromo.m_Adn)

            newInd1[corte:] = newInd2[corte:]            
            newInd2[corte:] = tmp[corte:]
        
        
        return ''.join(newInd1),''.join(newInd2)



    def nuevaPoblacion(self, individuos : tuple): 
        ##self.individuos = []
        self.m_Fenotipo_Total = 0
        for i in individuos:
            ind = Individuo()
            ind.nuevoIndividuo(i)
            self.individuos.append(ind)
            self.m_Fenotipo_Total += ind.m_Fenotipo
    

    def mutacion(self):
        n1 = randint(0,self.cant -1)
    
        m = randint(0, len(self.individuos[n1].m_Cromo.m_Adn) -1)
        if self.individuos[n1].m_Cromo.m_Adn[m] == '0':
            self.individuos[n1].m_Cromo.m_Adn =self.individuos[n1].m_Cromo.m_Adn[:m] + '1' + self.individuos[n1].m_Cromo.m_Adn[m+1:]
        else:
            self.individuos[n1].m_Cromo.m_Adn =self.individuos[n1].m_Cromo.m_Adn[:m] + '0' + self.individuos[n1].m_Cromo.m_Adn[m+1:]

    

    def imprime(self):
        
        self.individuos.sort(key=lambda x:x.m_Fenotipo,reverse=True)
        
        for i in self.individuos[:4]:
            print(i.m_Fenotipo)
    

    def plot(self,cont):

        fig = plt.figure()
        x = []
        y = []

        for i in range(self.cant):
            x.append(self.individuos[i].m_Genotipo)
            y.append(self.individuos[i].m_Fenotipo)
        

        plt.plot(x,y,'ro')
        plt.plot( [ x for x in range(31)] , [ x**2 for x in range(31    )] )
        fig.savefig('imagen_'+ str(cont)+ '.png')