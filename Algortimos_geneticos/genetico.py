from individuo import Poblacion
import random
import matplotlib.pyplot as plt
import copy

class AlgoritmoGenetico():

    poblacionOld = None
    poblacionNew = None
    mutacion = 0
    cant = 0
    def __init__(self,cant : int):
        self.cant = cant
        self.poblacionOld = Poblacion(cant,5)
        self.poblacionNew = Poblacion(cant,5)

        
    def iniciar(self):
        for j in range(5000):
            self.mutacion = random.random()

            self.poblacionOld.Calcular_Probalilidades()
            indList = self.CrossOver()

            self.poblacionNew.individuos=[]

            for i in range(len(indList)):
                self.poblacionNew.nuevaPoblacion(indList[i])

            #if(round(self.mutacion,1) < 1):
            self.poblacionNew.mutacion()


            if j%500 == 0:            
                self.poblacionNew.plot(j)

            self.poblacionOld = copy.deepcopy(self.poblacionNew)
            ##print("-------")
            self.poblacionOld.imprime()
    

    def CrossOver(self):
        ind = list()
        for _ in range(int(self.cant/2)):
            i = self.poblacionOld.CrossOver()    
            ind.append(i)
        return ind


def main():
    a = AlgoritmoGenetico(50)
    a.iniciar()
    #a.CrossOver()

if __name__ == '__main__':
    main()